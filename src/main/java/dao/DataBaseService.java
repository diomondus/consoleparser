package dao;

import entity.WordInfo;

public interface DataBaseService {

    void createTableIfNeed();

    WordInfo selectWordInformation(String word);

    void updateWordInformation(String word);

    void insertWordInformation(WordInfo wordInfo);
}