package service;

import com.google.inject.Inject;
import entity.WordInfo;
import lombok.RequiredArgsConstructor;
import repo.WordInfoRepository;

import java.util.Optional;

@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class WordHandlerImpl implements WordHandler{

    private final WordInfoRepository wordInfoRepository;

    @Override
    public void handleWord(String word) {
        Optional<WordInfo> optionalWord = wordInfoRepository.findByWord(word);
        if (optionalWord.isPresent()) {
            WordInfo w = optionalWord.get();
            wordInfoRepository.save(w.incrementWordCount());
        } else {
            WordInfo w = new WordInfo(word, 0);
            wordInfoRepository.create(w);
        }
    }
}